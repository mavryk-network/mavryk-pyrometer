export { NetworkConnection } from "./gen/NetworkConnection";
export { TezosVersion } from "./gen/TezosVersion";
export { BootstrappedStatus } from "./gen/BootstrappedStatus";

import { Participation as ParticipationI } from "./gen/Psithaca2MLR/Participation";
import { Participation as ParticipationO } from "./gen/ProxfordYmVf/Participation";
import { Participation as ParticipationP } from "./gen/PtParisBxoLz/Participation";

import { ConsensusKey as ConsensusKeyO } from "./gen/ProxfordYmVf/ConsensusKey";
import { ConsensusKey as ConsensusKeyP } from "./gen/PtParisBxoLz/ConsensusKey";

import { Delegate as DelegateH } from "./gen/Delegate";
import { Delegate as DelegateI } from "./gen/Delegate";
import { Delegate as DelegateJ } from "./gen/Delegate";
import { Delegate as DelegateK } from "./gen/PtKathmankSp/Delegate";
import { Delegate as DelegateL } from "./gen/PtLimaPtLMwf/Delegate";
import { Delegate as DelegateM } from "./gen/PtMumbai2Tms/Delegate";
import { Delegate as DelegateN } from "./gen/PtNairobiyss/Delegate";
import { Delegate as DelegateO } from "./gen/ProxfordYmVf/Delegate";
import { Delegate as DelegateP } from "./gen/PtParisBxoLz/Delegate";

import { ShellHeader as BlockHeaderH } from "./gen/PtHangz2aRng/BlockHeader";
import { ShellHeader as BlockHeaderI } from "./gen/Psithaca2MLR/BlockHeader";
import { ShellHeader as BlockHeaderJ } from "./gen/PtJakart2xVj/BlockHeader";
import { ShellHeader as BlockHeaderK } from "./gen/PtKathmankSp/BlockHeader";
import { ShellHeader as BlockHeaderL } from "./gen/PtLimaPtLMwf/BlockHeader";
import { ShellHeader as BlockHeaderM } from "./gen/PtMumbai2Tms/BlockHeader";
import { ShellHeader as BlockHeaderN } from "./gen/PtNairobiyss/BlockHeader";
import { ShellHeader as BlockHeaderO } from "./gen/ProxfordYmVf/BlockHeader";
import { ShellHeader as BlockHeaderP } from "./gen/PtParisBxoLz/BlockHeader";

import { EndorsingRights as EndorsingRightsH } from "./gen/PtHangz2aRng/EndorsingRights";
import { EndorsingRights as EndorsingRightsI } from "./gen/Psithaca2MLR/EndorsingRights";
import { EndorsingRights as EndorsingRightsJ } from "./gen/PtJakart2xVj/EndorsingRights";
import { EndorsingRights as EndorsingRightsK } from "./gen/PtKathmankSp/EndorsingRights";
import { EndorsingRights as EndorsingRightsL } from "./gen/PtLimaPtLMwf/EndorsingRights";
import { EndorsingRights as EndorsingRightsM } from "./gen/PtMumbai2Tms/EndorsingRights";
import { EndorsingRights as EndorsingRightsN } from "./gen/PtNairobiyss/EndorsingRights";
import { EndorsingRights as EndorsingRightsO } from "./gen/ProxfordYmVf/EndorsingRights";
import { EndorsingRights as EndorsingRightsP } from "./gen/PtParisBxoLz/EndorsingRights";

import { Constants as ConstantsH } from "./gen/PtHangz2aRng/Constants";
import { Constants as ConstantsI } from "./gen/Psithaca2MLR/Constants";
import { Constants as ConstantsJ } from "./gen/PtJakart2xVj/Constants";
import { Constants as ConstantsK } from "./gen/PtKathmankSp/Constants";
import { Constants as ConstantsL } from "./gen/PtLimaPtLMwf/Constants";
import { Constants as ConstantsM } from "./gen/PtMumbai2Tms/Constants";
import { Constants as ConstantsN } from "./gen/PtNairobiyss/Constants";
import { Constants as ConstantsO } from "./gen/ProxfordYmVf/Constants";
import { Constants as ConstantsP } from "./gen/PtParisBxoLz/Constants";

import { BakingRights as BakingRightsH } from "./gen/PtHangz2aRng/BakingRights";
import { BakingRights as BakingRightsI } from "./gen/Psithaca2MLR/BakingRights";
import { BakingRights as BakingRightsJ } from "./gen/PtJakart2xVj/BakingRights";
import { BakingRights as BakingRightsK } from "./gen/PtKathmankSp/BakingRights";
import { BakingRights as BakingRightsL } from "./gen/PtLimaPtLMwf/BakingRights";
import { BakingRights as BakingRightsM } from "./gen/PtMumbai2Tms/BakingRights";
import { BakingRights as BakingRightsN } from "./gen/PtNairobiyss/BakingRights";
import { BakingRights as BakingRightsO } from "./gen/ProxfordYmVf/BakingRights";
import { BakingRights as BakingRightsP } from "./gen/PtParisBxoLz/BakingRights";

import { Block as BlockH } from "./gen/PtHangz2aRng/Block";
import { Block as BlockI } from "./gen/Psithaca2MLR/Block";
import { Block as BlockJ } from "./gen/PtJakart2xVj/Block";
import { Block as BlockK } from "./gen/PtKathmankSp/Block";
import { Block as BlockL } from "./gen/PtLimaPtLMwf/Block";
import { Block as BlockM } from "./gen/PtMumbai2Tms/Block";
import { Block as BlockN } from "./gen/PtNairobiyss/Block";
import { Block as BlockO } from "./gen/ProxfordYmVf/Block";
import { Block as BlockP } from "./gen/PtParisBxoLz/Block";

import { Operation as OperationH } from "./gen/PtHangz2aRng/Block";
import { Operation as OperationI } from "./gen/Psithaca2MLR/Block";
import { Operation as OperationJ } from "./gen/PtJakart2xVj/Block";
import { Operation as OperationK } from "./gen/PtKathmankSp/Block";
import { Operation as OperationL } from "./gen/PtLimaPtLMwf/Block";
import { Operation as OperationM } from "./gen/PtMumbai2Tms/Block";
import { Operation as OperationN } from "./gen/PtNairobiyss/Block";
import { Operation as OperationO } from "./gen/ProxfordYmVf/Block";
import { Operation as OperationP } from "./gen/PtParisBxoLz/Block";

import {
  DoubleBakingEvidence1 as DoubleBakingEvidenceH,
  DoubleEndorsementEvidence1 as DoubleEndorsementEvidenceH,
  EndorsementWithSlot1 as EndorsementWithSlotH,
} from "./gen/PtHangz2aRng/Block";

import {
  DoubleBakingEvidence1 as DoubleBakingEvidenceI,
  DoubleEndorsementEvidence1 as DoubleEndorsementEvidenceI,
  Endorsement1 as EndorsementWithSlotI,
} from "./gen/Psithaca2MLR/Block";

import {
  DoubleBakingEvidence1 as DoubleBakingEvidenceJ,
  DoubleEndorsementEvidence1 as DoubleEndorsementEvidenceJ,
  Endorsement1 as EndorsementWithSlotJ,
} from "./gen/PtJakart2xVj/Block";

import {
  DoubleBakingEvidence1 as DoubleBakingEvidenceK,
  DoubleEndorsementEvidence1 as DoubleEndorsementEvidenceK,
  Endorsement1 as EndorsementWithSlotK,
} from "./gen/PtKathmankSp/Block";

import {
  DoubleBakingEvidence1 as DoubleBakingEvidenceL,
  DoubleEndorsementEvidence1 as DoubleEndorsementEvidenceL,
  Endorsement1 as EndorsementWithSlotL,
} from "./gen/PtLimaPtLMwf/Block";

import {
  DoubleBakingEvidence1 as DoubleBakingEvidenceM,
  DoubleEndorsementEvidence1 as DoubleEndorsementEvidenceM,
  Endorsement1 as EndorsementWithSlotM,
} from "./gen/PtMumbai2Tms/Block";

import {
  DoubleBakingEvidence1 as DoubleBakingEvidenceN,
  DoubleEndorsementEvidence1 as DoubleEndorsementEvidenceN,
  Endorsement1 as EndorsementWithSlotN,
} from "./gen/PtNairobiyss/Block";

import {
  DoubleBakingEvidence1 as DoubleBakingEvidenceO,
  DoubleEndorsementEvidence1 as DoubleEndorsementEvidenceO,
  Endorsement1 as EndorsementWithSlotO,
} from "./gen/ProxfordYmVf/Block";

import {
  DoubleBakingEvidence1 as DoubleBakingEvidenceP,
  DoubleEndorsementEvidence1 as DoubleEndorsementEvidenceP,
  Endorsement1 as EndorsementWithSlotP,
} from "./gen/PtParisBxoLz/Block";

export { ParticipationI, ParticipationO, ParticipationP };

export type Participation = ParticipationI | ParticipationO | ParticipationP;

export type ConsensusKeyM = {
  active: string;
  pendings?: { cycle: number; pkh: string }[];
};

export { ConsensusKeyO, ConsensusKeyP };

export type ConsensusKey = ConsensusKeyM | ConsensusKeyO | ConsensusKeyP;

export {
  DelegateH,
  DelegateI,
  DelegateJ,
  DelegateK,
  DelegateL,
  DelegateM,
  DelegateN,
  DelegateO,
  DelegateP,
};
export type Delegate =
  | DelegateH
  | DelegateI
  | DelegateJ
  | DelegateK
  | DelegateL
  | DelegateM
  | DelegateN
  | DelegateO
  | DelegateP;

export { BlockH, BlockI, BlockJ, BlockK, BlockL, BlockM, BlockN, BlockO, BlockP };
export type Block =
  | BlockH
  | BlockI
  | BlockJ
  | BlockK
  | BlockL
  | BlockM
  | BlockN
  | BlockO
  | BlockP;

export {
  BlockHeaderH,
  BlockHeaderI,
  BlockHeaderJ,
  BlockHeaderK,
  BlockHeaderL,
  BlockHeaderM,
  BlockHeaderN,
  BlockHeaderO,
  BlockHeaderP,
};
export type BlockHeader =
  | BlockHeaderH
  | BlockHeaderI
  | BlockHeaderJ
  | BlockHeaderK
  | BlockHeaderL
  | BlockHeaderM
  | BlockHeaderN
  | BlockHeaderO
  | BlockHeaderP;

export type EndorsingRightH = EndorsingRightsH[number];
export type EndorsingRightI = EndorsingRightsI[number];
export type EndorsingRightJ = EndorsingRightsJ[number];
export type EndorsingRightK = EndorsingRightsK[number];
export type EndorsingRightL = EndorsingRightsL[number];
export type EndorsingRightM = EndorsingRightsM[number];
export type EndorsingRightN = EndorsingRightsN[number];
export type EndorsingRightO = EndorsingRightsO[number];
export type EndorsingRightP = EndorsingRightsP[number];
export type {
  EndorsingRightsH,
  EndorsingRightsI,
  EndorsingRightsJ,
  EndorsingRightsK,
  EndorsingRightsL,
  EndorsingRightsM,
  EndorsingRightsN,
  EndorsingRightsO,
  EndorsingRightsP,
};
export type EndorsingRight =
  | EndorsingRightH
  | EndorsingRightI
  | EndorsingRightJ
  | EndorsingRightK
  | EndorsingRightL
  | EndorsingRightM
  | EndorsingRightN
  | EndorsingRightO
  | EndorsingRightP;
export type EndorsingRights =
  | EndorsingRightsH
  | EndorsingRightsI
  | EndorsingRightsJ
  | EndorsingRightsK
  | EndorsingRightsL
  | EndorsingRightsM
  | EndorsingRightsN
  | EndorsingRightsO
  | EndorsingRightsP;

export type BakingRightH = BakingRightsH[number];
export type BakingRightI = BakingRightsI[number];
export type BakingRightJ = BakingRightsI[number];
export type BakingRightK = BakingRightsK[number];
export type BakingRightL = BakingRightsL[number];
export type BakingRightM = BakingRightsM[number];
export type BakingRightN = BakingRightsN[number];
export type BakingRightO = BakingRightsO[number];
export type BakingRightP = BakingRightsP[number];
export type {
  BakingRightsH,
  BakingRightsI,
  BakingRightsJ,
  BakingRightsK,
  BakingRightsL,
  BakingRightsM,
  BakingRightsN,
  BakingRightsO,
  BakingRightsP,
};
export type BakingRights =
  | BakingRightsH
  | BakingRightsI
  | BakingRightsJ
  | BakingRightsK
  | BakingRightsL
  | BakingRightsM
  | BakingRightsN
  | BakingRightsO
  | BakingRightsP;
export type BakingRight =
  | BakingRightH
  | BakingRightI
  | BakingRightJ
  | BakingRightK
  | BakingRightL
  | BakingRightM
  | BakingRightN
  | BakingRightO
  | BakingRightP;

export type Constants =
  | ConstantsH
  | ConstantsI
  | ConstantsJ
  | ConstantsK
  | ConstantsL
  | ConstantsM
  | ConstantsN
  | ConstantsO
  | ConstantsP;

export type {
  OperationH,
  OperationI,
  OperationJ,
  OperationK,
  OperationL,
  OperationM,
  OperationN,
  OperationO,
  OperationP,
};
export type OperationEntry =
  | OperationH
  | OperationI
  | OperationJ
  | OperationK
  | OperationL
  | OperationM
  | OperationN
  | OperationO
  | OperationP

export type DoubleBakingEvidence =
  | DoubleBakingEvidenceH
  | DoubleBakingEvidenceI
  | DoubleBakingEvidenceJ
  | DoubleBakingEvidenceK
  | DoubleBakingEvidenceL
  | DoubleBakingEvidenceM
  | DoubleBakingEvidenceN
  | DoubleBakingEvidenceO
  | DoubleBakingEvidenceP;

export type { DoubleBakingEvidenceH };
export type { DoubleBakingEvidenceI };
export type { DoubleBakingEvidenceJ };
export type { DoubleBakingEvidenceK };
export type { DoubleBakingEvidenceL };
export type { DoubleBakingEvidenceM };
export type { DoubleBakingEvidenceN };
export type { DoubleBakingEvidenceO };
export type { DoubleBakingEvidenceP };

export type DoubleEndorsementEvidence =
  | DoubleEndorsementEvidenceH
  | DoubleEndorsementEvidenceI
  | DoubleEndorsementEvidenceJ
  | DoubleEndorsementEvidenceK
  | DoubleEndorsementEvidenceL
  | DoubleEndorsementEvidenceM
  | DoubleEndorsementEvidenceN
  | DoubleEndorsementEvidenceO
  | DoubleEndorsementEvidenceP

export {
  DoubleEndorsementEvidenceH,
  DoubleEndorsementEvidenceI,
  DoubleEndorsementEvidenceJ,
  DoubleEndorsementEvidenceK,
  DoubleEndorsementEvidenceL,
  DoubleEndorsementEvidenceM,
  DoubleEndorsementEvidenceN,
  DoubleEndorsementEvidenceO,
  DoubleEndorsementEvidenceP,
};

export type EndorsementWithSlot =
  | EndorsementWithSlotH
  | EndorsementWithSlotI
  | EndorsementWithSlotJ
  | EndorsementWithSlotK
  | EndorsementWithSlotL
  | EndorsementWithSlotM
  | EndorsementWithSlotN
  | EndorsementWithSlotO
  | EndorsementWithSlotP;

export {
  EndorsementWithSlotH,
  EndorsementWithSlotI,
  EndorsementWithSlotJ,
  EndorsementWithSlotK,
  EndorsementWithSlotL,
  EndorsementWithSlotM,
  EndorsementWithSlotN,
  EndorsementWithSlotO,
  EndorsementWithSlotP,
};

export enum OpKind {
  ORIGINATION = "origination",
  DELEGATION = "delegation",
  REVEAL = "reveal",
  TRANSACTION = "transaction",
  ACTIVATION = "activate_account",
  ENDORSEMENT = "endorsement",
  ATTESTATION = "attestation",
  ENDORSEMENT_WITH_SLOT = "endorsement_with_slot",
  SEED_NONCE_REVELATION = "seed_nonce_revelation",
  DOUBLE_ENDORSEMENT_EVIDENCE = "double_endorsement_evidence",
  DOUBLE_ATTESTATION_EVIDENCE = "double_attestation_evidence",
  DOUBLE_PREENDORSEMENT_EVIDENCE = "double_preendorsement_evidence",
  DOUBLE_PREATTESTATION_EVIDENCE = "double_preattestation_evidence",
  DOUBLE_BAKING_EVIDENCE = "double_baking_evidence",
  PROPOSALS = "proposals",
  BALLOT = "ballot",
  FAILING_NOOP = "failing_noop",
}

export type TzAddress = string;
export type URL = string;
